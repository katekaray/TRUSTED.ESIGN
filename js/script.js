 setInterval(function() {showSlides(1)},10000);
 
function animate(draw, duration) {
	var start = performance.now();
	requestAnimationFrame(function animate(time) {
		var timePassed = time - start;
		if (timePassed > duration) timePassed = duration;
		draw(timePassed);
		if (timePassed < duration) {
		  requestAnimationFrame(animate);
		}
	});
}

function showSlides(n){
	var sec=1000, d_x1, d_y1, d_x2, d_x3, d_y3, s1, s2, s3;
	var slides = document.getElementsByClassName("slide");
	var x = slider.getBoundingClientRect().x;
	var y = slider.getBoundingClientRect().y;
	var width = slider.getBoundingClientRect().width;
	var height = slider.getBoundingClientRect().height;
	if (n==1) {
		d_x1 = Math.abs(width - slides[0].getBoundingClientRect().width)/2;//+ slides[0].style.marginLeft);
		d_y1 = Math.abs(height - slides[0].getBoundingClientRect().height)/2;
		d_x2 = slides[1].getBoundingClientRect().width;//+ slides[1].style.marginLeft);
		d_x3 = Math.abs(width - slides[2].getBoundingClientRect().width)/2;
		d_y3 = Math.abs(height - slides[2].getBoundingClientRect().height)/2;
		s1=slides[0].innerHTML;
		s2=slides[1].innerHTML;
		s3=slides[2].innerHTML;
		slides[0].innerHTML=s3;
		slides[0].style.left = d_x3 + 'px';
		slides[0].style.bottom = d_y3 + 'px';
		slides[1].innerHTML=s1;
		slides[1].style.left = d_x1 + 'px';
		slides[1].style.top = d_y1 + 'px';
		slides[2].innerHTML=s2;
		slides[2].style.right = d_x2 + 'px';
		animate(function(timePassed) {
			slides[1].style.left = d_x1 - timePassed / (sec / d_x1) + 'px';
			slides[1].style.top = d_y1 - timePassed / (sec / d_y1) + 'px';
			slides[2].style.right = d_x2 - timePassed / (sec / d_x2) + 'px';
			slides[0].style.left = d_x3 - timePassed / (sec / d_x3) + 'px';
			slides[0].style.bottom = d_y3 - timePassed / (sec / d_y3) + 'px';
		}, sec);
	} else if (n==-1)
	{
		d_x1 = Math.abs(width - slides[0].getBoundingClientRect().width)/2;
		d_y1 = Math.abs(height - slides[0].getBoundingClientRect().height)/2;
		d_x2 = Math.abs(width - slides[1].getBoundingClientRect().width)/2;
		d_y2 = Math.abs(height - slides[1].getBoundingClientRect().height)/2;
		d_x3 = slides[1].getBoundingClientRect().width;
		s1=slides[0].innerHTML;
		s2=slides[1].innerHTML;
		s3=slides[2].innerHTML;
		animate(function(timePassed) {
			slides[0].style.left = timePassed / (sec / d_x1) + 'px';
			slides[0].style.bottom = timePassed / (sec / d_y1) + 'px';
			slides[1].style.left = timePassed / (sec / d_x2) + 'px';
			slides[1].style.top = timePassed / (sec / d_y2) + 'px';
			slides[2].style.right = timePassed / (sec / d_x3) + 'px';
			if (timePassed==sec) {
				slides[0].innerHTML=s2;
				slides[0].style.left = '0px';
				slides[0].style.bottom = '0px';
				slides[1].innerHTML=s3;
				slides[1].style.left = '0px';
				slides[1].style.top = '0px';
				slides[2].innerHTML=s1;		
				slides[2].style.right = '0px'		
			}
		}, sec);
	}
}